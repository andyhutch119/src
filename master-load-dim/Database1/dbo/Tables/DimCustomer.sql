﻿CREATE TABLE [dbo].[DimCustomer](
	[CustomerSK] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [nvarchar](50) NOT NULL,
	[CustomerName] [nvarchar](255) NOT NULL,
	[City] [nvarchar](150) NULL,
	[Channel] [nvarchar](100) NULL,
	[SubChannel] [nvarchar](50) NULL,
	[CatCode09] [nvarchar](50) NULL,
	[CatCode20] [nvarchar](50) NULL,
	[SellingRegion] [nvarchar](100) NULL,
	[DemandGroup] [nvarchar](100) NULL,
	[ShopperChannel] [nvarchar](100) NULL,
	[SalesUnit] [nvarchar](100) NULL,
	[Type] [nvarchar](50) NULL,
	[GrowthStrategy] [nvarchar](100) NULL,
	[GeographyId] [int] NULL,
	[BannerSK] [int] NULL,
	[InboundEmployeeSK] [int] NULL,
	[OutboundEmployeeSK] [int] NULL,
	[BusinessUnitID] [nvarchar](50) NULL,
	[LastOrderNo] [nvarchar](50) NULL,
	[LastOrderDate] [date] NULL,
	[AvgDaysToOrder(Last2Yrs)] [int] NULL,
	[IsActive] [nvarchar](50) NULL,
	[IsDeleted] [nvarchar](50) NULL,
	[InsertDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[InsertAuditKey] [int] NOT NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
 CONSTRAINT [CustomerSK] PRIMARY KEY CLUSTERED 
(
	[CustomerSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO

ALTER TABLE [dbo].[DimCustomer]  WITH CHECK ADD  CONSTRAINT [FK_DimCustomer_DimGeography] FOREIGN KEY([GeographyId])
REFERENCES [dbo].[DimGeography] ([GeographyId])
GO

ALTER TABLE [dbo].[DimCustomer] CHECK CONSTRAINT [FK_DimCustomer_DimGeography]
GO