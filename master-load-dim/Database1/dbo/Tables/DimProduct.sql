﻿
CREATE TABLE [dbo].[DimProduct](
	[ProductSK] [int] IDENTITY(1,1) NOT NULL,
	[ProductSKU] [nvarchar](100) NOT NULL,
	[ProductName] [nvarchar](255) NULL,
	[ProductFamily] [nvarchar](100) NULL,
	[ProductSegment] [nvarchar](100) NULL,
	[ProductCondition] [nvarchar](100) NULL,
	[ProductGroup] [nvarchar](100) NULL,
	[ProductClass] [nvarchar](100) NULL,
	[StockType] [nvarchar](50) NULL,
	[ProductStrategy] [nvarchar](50) NULL,
	[CommodityClass] [nvarchar](50) NULL,
	[CommoditySubClass] [nvarchar](50) NULL,
	[ShopperSegment] [nvarchar](100) NULL,
	[ConsumerGroup] [nvarchar](100) NULL,
	[BusinessPriority] [nvarchar](100) NULL,
	[ProductImageURL] [nvarchar](500) NULL,
	[CompanyBrandSK] [int] NULL,
	[MasterProductSK] [int] NULL,
	[MasterProductName] [nvarchar](255) NULL,
	[AztecConsumerSegment] [nvarchar](50) NULL,
	[AztecShopperSegment] [nvarchar](50) NULL,
	[IsActive] [nvarchar](50) NULL,
	[IsDeleted] [nvarchar](50) NULL,
	[InsertDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[InsertAuditKey] [int] NOT NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
 CONSTRAINT [PK_DimProduct] PRIMARY KEY CLUSTERED 
(
	[ProductSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)