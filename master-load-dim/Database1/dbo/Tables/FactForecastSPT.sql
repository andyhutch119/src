﻿CREATE TABLE [dbo].[FactForecastSPT](
	[ForecastSK] [int] NOT NULL,
	[DateSK] [int] NOT NULL,
	[CustomerSK] [int] NOT NULL,
	[ProductSK] [int] NOT NULL,
	[InboundEmployeeSK] [int] NOT NULL,
	[OutboundEmployeeSK] [int] NOT NULL,
	[ListPriceForecastSPT] [decimal](19, 4) NULL,
	[InvoicedSalesForecastSPT] [decimal](19, 4) NULL,
	[COGSForecastSPT] [decimal](19, 4) NULL,
	[GrossMarginForecastSPT] [decimal](19, 4) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FactForecastSPT] PRIMARY KEY CLUSTERED 
(
	[DateSK] ASC,
	[CustomerSK] ASC,
	[ProductSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
ALTER TABLE [dbo].[FactForecastSPT]  WITH CHECK ADD  CONSTRAINT [FK_FactForecastSPT_DimCustomer] FOREIGN KEY([CustomerSK])
REFERENCES [dbo].[DimCustomer] ([CustomerSK])
GO

ALTER TABLE [dbo].[FactForecastSPT] CHECK CONSTRAINT [FK_FactForecastSPT_DimCustomer]
GO
ALTER TABLE [dbo].[FactForecastSPT]  WITH CHECK ADD  CONSTRAINT [FK_FactForecastSPT_DimDate] FOREIGN KEY([DateSK])
REFERENCES [dbo].[DimDate] ([DateSK])
GO

ALTER TABLE [dbo].[FactForecastSPT] CHECK CONSTRAINT [FK_FactForecastSPT_DimDate]
GO
ALTER TABLE [dbo].[FactForecastSPT]  WITH CHECK ADD  CONSTRAINT [FK_FactForecastSPT_DimProduct] FOREIGN KEY([ProductSK])
REFERENCES [dbo].[DimProduct] ([ProductSK])
GO

ALTER TABLE [dbo].[FactForecastSPT] CHECK CONSTRAINT [FK_FactForecastSPT_DimProduct]
GO
ALTER TABLE [dbo].[FactForecastSPT]  WITH CHECK ADD  CONSTRAINT [FK_FactForecastSPT_InboundEmployee] FOREIGN KEY([InboundEmployeeSK])
REFERENCES [dbo].[DimEmployee] ([EmployeeSK])
GO

ALTER TABLE [dbo].[FactForecastSPT] CHECK CONSTRAINT [FK_FactForecastSPT_InboundEmployee]
GO
ALTER TABLE [dbo].[FactForecastSPT]  WITH CHECK ADD  CONSTRAINT [FK_FactForecastSPT_OutboundEmployee] FOREIGN KEY([OutboundEmployeeSK])
REFERENCES [dbo].[DimEmployee] ([EmployeeSK])
GO

ALTER TABLE [dbo].[FactForecastSPT] CHECK CONSTRAINT [FK_FactForecastSPT_OutboundEmployee]