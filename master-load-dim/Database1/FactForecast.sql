﻿
CREATE TABLE [dbo].[FactForecast](
	[ForecastSK] [int] NOT NULL,
	[DateSK] [int] NOT NULL,
	[CustomerSK] [int] NOT NULL,
	[ProductSK] [int] NOT NULL,
	[InboundEmployeeSK] [int] NOT NULL,
	[OutboundEmployeeSK] [int] NOT NULL,
	[UnitForecast] [decimal](19, 4) NULL,
	[InvoicedSalesForecast] [decimal](19, 4) NULL,
	[COGForecast] [decimal](19, 4) NULL,
	[ListPriceForecast] [decimal](19, 4) NULL,
	[GrossMarginForecast] [decimal](19, 4) NULL,
	[ManualRebateForecast] [decimal](19, 4) NULL,
	[NetSalesForecast] [decimal](19, 4) NULL,
	[TradeSpend] [decimal](19, 4) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FactForecast] PRIMARY KEY CLUSTERED 
(
	[DateSK] ASC,
	[CustomerSK] ASC,
	[ProductSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO

ALTER TABLE [dbo].[FactForecast]  WITH CHECK ADD  CONSTRAINT [FK_FactForecast_DimCustomer] FOREIGN KEY([CustomerSK])
REFERENCES [dbo].[DimCustomer] ([CustomerSK])
GO

ALTER TABLE [dbo].[FactForecast] CHECK CONSTRAINT [FK_FactForecast_DimCustomer]
GO

ALTER TABLE [dbo].[FactForecast]  WITH CHECK ADD  CONSTRAINT [FK_FactForecast_DimDate] FOREIGN KEY([DateSK])
REFERENCES [dbo].[DimDate] ([DateSK])
GO

ALTER TABLE [dbo].[FactForecast] CHECK CONSTRAINT [FK_FactForecast_DimDate]
GO

ALTER TABLE [dbo].[FactForecast]  WITH CHECK ADD  CONSTRAINT [FK_FactForecast_DimProduct] FOREIGN KEY([ProductSK])
REFERENCES [dbo].[DimProduct] ([ProductSK])
GO

ALTER TABLE [dbo].[FactForecast] CHECK CONSTRAINT [FK_FactForecast_DimProduct]
GO

ALTER TABLE [dbo].[FactForecast]  WITH CHECK ADD  CONSTRAINT [FK_FactForecast_InboundEmployee] FOREIGN KEY([InboundEmployeeSK])
REFERENCES [dbo].[DimEmployee] ([EmployeeSK])
GO

ALTER TABLE [dbo].[FactForecast] CHECK CONSTRAINT [FK_FactForecast_InboundEmployee]
GO

ALTER TABLE [dbo].[FactForecast]  WITH CHECK ADD  CONSTRAINT [FK_FactForecast_OutboundEmployee] FOREIGN KEY([OutboundEmployeeSK])
REFERENCES [dbo].[DimEmployee] ([EmployeeSK])
GO

ALTER TABLE [dbo].[FactForecast] CHECK CONSTRAINT [FK_FactForecast_OutboundEmployee]
GO