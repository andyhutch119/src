﻿
CREATE TABLE [dbo].[Customer](
	[CustomerID] [nvarchar](50) NOT NULL,
	[CustomerName] [nvarchar](255) NOT NULL,
	[City] [nvarchar](100) NULL,
	[Channel] [nvarchar](100) NULL,
	[SubChannel] [nvarchar](50) NULL,
	[CatCode09] [nvarchar](50) NULL,
	[CatCode20] [nvarchar](50) NULL,
	[SellingRegion] [nvarchar](100) NULL,
	[DemandGroup] [nvarchar](100) NULL,
	[ShopperChannel] [nvarchar](100) NULL,
	[SalesUnit] [nvarchar](100) NULL,
	[Type] [nvarchar](50) NULL,
	[GrowthStrategy] [nvarchar](100) NULL,
	[GeographyId] [int] NULL,
	[BannerCode] [nvarchar](50) NOT NULL,
	[InboundEmployeeID] [nvarchar](50) NULL,
	[OutboundEmployeeID] [nvarchar](50) NULL,
	[BusinessUnitID] [nvarchar](50) NULL,
	[LastOrderNo] [nvarchar](50) NULL,
	[LastOrderDate] [date] NULL,
	[AvgDaysToOrder(Last2Yrs)] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[IsActive] [varchar](50) NULL,
	[InsertDate] [Datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [Datetime] NULL,
	[UpdateAuditKey] [int] NULL
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[SourceRegion] ASC,
	[SourceProvider] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)




