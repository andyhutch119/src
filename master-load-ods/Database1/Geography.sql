﻿
CREATE TABLE [dbo].[Geography](
	[GeographyId] [int] NOT NULL,
	[PostCode] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[CountryName] [varchar](50) NULL,
	[PopulationCount] [int] NULL,
	[Area sqkm] [decimal](10, 2) NULL,
	[Latitude] [decimal](10, 6) NULL,
	[Longitude] [decimal](10, 6) NULL,
	[CountryFlagURL] [nvarchar](500) NULL,
	[InsertDate] DATETIME NULL, 
    [InsertAuditKey] INT NULL, 
    [UpdateDate] DATETIME NULL, 
    [UpdateAuditKey] INT NULL, 
 CONSTRAINT [PK_Geography] PRIMARY KEY CLUSTERED 
(
	[GeographyId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)