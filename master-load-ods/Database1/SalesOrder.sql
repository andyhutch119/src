﻿CREATE TABLE [dbo].[SalesOrder](
	[InvoiceDateSK] [int] NOT NULL,
	[CustomerID] [nvarchar](50) NOT NULL,
	[ProductSKU] [nvarchar](100) NOT NULL,
	[BranchPlantCode] [nvarchar](50) NOT NULL,
	[InboundEmployeeID] [nvarchar](50) NULL,
	[OutBoundEmployeeID] [nvarchar](50) NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[OrderType] [nvarchar](50) NOT NULL,
	[LineID] [decimal](6, 0) NOT NULL,
	[Unit] [int] NULL,
	[COG] [decimal](19, 4) NULL,
	[InvoiceSale] [decimal](19, 4) NULL,
	[ListPrice] [decimal](19, 4) NULL,
	[GrossMargin] [decimal](19, 4) NULL,
	[CurrencyPair] [nvarchar](10) NOT NULL,
	[ExchangeRate] [decimal](19, 4) NOT NULL,
	[ReturnCount] [int] NULL,
	[ReturnValue] [decimal](19, 4) NULL,
	[OID] [decimal](19, 4) NULL,
	[ManualRebate] [decimal](19, 4) NULL,
	[NetSales] [decimal](19, 4) NULL,
	[TradeSpend] [decimal](19, 4) NULL,
	[TransSK] [int] NULL,
	[ShipmentNo] [nvarchar](50) NULL,
	[Backordered/Cancelled] [nvarchar](20) NULL,
	[OrderDateSK] [int] NOT NULL,
	[Rout] [nvarchar](250) NULL,
	[ShipmentDateSK] [int] NULL,
	[BackorderReleaseDateSK] [int] NULL,
	[LineType] [nvarchar](20) NULL,
	[QuantityOrdered] [int] NULL,
	[QuantityShipped] [int] NULL,
	[QuantityCancelled] [int] NULL,
	[QuantityBackordered] [int] NULL,
	[UOM] [nvarchar](20) NULL,
	[CartonOrdered] [int] NULL,
	[CartonShipped] [int] NULL,
	[CartonCancelled] [int] NULL,
	[CartonBackordered] [int] NULL,
	[UnitOrdered] [int] NULL,
	[UnitShipped] [int] NULL,
	[UnitCancelled] [int] NULL,
	[UnitBackordered] [int] NULL,
	[ValueOrdered] [decimal](19, 4) NULL,
	[ValueShipped] [decimal](19, 4) NULL,
	[ValueCancelled] [decimal](19, 4) NULL,
	[ValueBackordered] [decimal](19, 4) NULL,
	[Live] [bit] null,
	[HoldCode] [nvarchar](50) NULL,
	[EstimatedInvoicedSales] [decimal](19,4) NULL,
	[LiveClassification] [NVARCHAR](50) NULL,
	[OrderSK] [int] NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[InsertAuditKey] [int] NOT NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SalesOrder] PRIMARY KEY CLUSTERED 
(
	[OrderDateSK] ASC,
	[CustomerID] ASC,
	[BranchPlantCode] ASC,
	[ProductSKU] ASC,
	[OrderNo] ASC,
	[OrderType] ASC,
	[LineID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO

ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_BranchPlant] FOREIGN KEY([BranchPlantCode], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[BranchPlant] ([BranchPlantCode], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_BranchPlant]
GO

ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_Customer] FOREIGN KEY([CustomerID], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[Customer] ([CustomerID], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_Customer]
GO



ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_InboundEmployee] FOREIGN KEY([InboundEmployeeID], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[Employee] ([EmployeeID], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_InboundEmployee]
GO

ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_OutboundEmployee] FOREIGN KEY([OutBoundEmployeeID], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[Employee] ([EmployeeID], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_OutboundEmployee]
GO

ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_Product] FOREIGN KEY([ProductSKU], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[Product] ([ProductSKU], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_Product]
GO
