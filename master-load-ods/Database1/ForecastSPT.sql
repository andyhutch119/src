﻿CREATE TABLE [dbo].[ForecastSPT](
	[ForecastSK] [int] NULL,
	[DateSK] [int] NULL,
	[CustomerID] [nvarchar](50) NOT NULL,
	[ProductSKU] [nvarchar](100) NOT NULL,
	[InboundEmployeeID] [nvarchar](50) NULL,
	[OutboundEmployeeID] [nvarchar](50) NULL,
	[ListPriceForecastSPT] [decimal](19, 4) NULL,
	[InvoicedSalesForecastSPT] [decimal](19, 4) NULL,
	[COGSForecastSPT] [decimal](19, 4) NULL,
	[GrossMarginForecastSPT] [decimal](19, 4) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL
)