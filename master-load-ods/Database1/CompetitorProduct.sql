﻿
CREATE TABLE [dbo].[CompetitorProduct](
	[ProductSKU] [nvarchar](100) NOT NULL,
	[ProductName] [nvarchar](255) NULL,
	[CompetitorProductSKU] [nvarchar](100) NOT NULL,
	[CompetitorProductName] [nvarchar](255) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[CompetitorProductSourceRegion] [nvarchar](50) NOT NULL,
	[CompetitorProductSourceProvider] [nvarchar](50) NOT NULL,
	[InsertDate] DATETIME NULL, 
    [InsertAuditKey] INT NULL, 
    [UpdateDate] DATETIME NULL, 
    [UpdateAuditKey] INT NULL, 
 CONSTRAINT [PK_CompetitorProduct] PRIMARY KEY CLUSTERED 
(
	[ProductSKU] ASC,
	[SourceRegion] ASC,
	[CompetitorProductSKU] ASC,
	[CompetitorProductSourceRegion] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO


