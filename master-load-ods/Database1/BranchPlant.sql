﻿
CREATE TABLE [dbo].[BranchPlant](
	[BranchPlantCode] [nvarchar](50) NOT NULL,
	[BranchPlantName] [nvarchar](100) NULL,
	[CompanyCode] [nvarchar](255) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[IsActive] [varchar](50) NULL,
	[InsertDate] [Datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [Datetime] NULL,
	[UpdateAuditKey] [int] NULL
 CONSTRAINT [PK_BranchPlant] PRIMARY KEY CLUSTERED 
(
	[BranchPlantCode] ASC,
	[SourceRegion] ASC,
	[SourceProvider] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO