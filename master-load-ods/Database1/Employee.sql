﻿
CREATE TABLE [dbo].[Employee](
	[EmployeeID] [nvarchar](50) NOT NULL,
	[EmployeeName] [nvarchar](150) NULL,
	[EmployeeType] [nvarchar](50) NULL,
	[TeamCode] [nvarchar](50) NULL,
	[CatCode] [nvarchar](50) NULL,
	[ManagerID] [nvarchar](50) NULL,
	[ManagerName] [nvarchar](150) NULL,
	[ManagerSourceRegion] [nvarchar](50) NULL,
	[ManagerSourceProvider] [nvarchar](50) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[IsActive] [varchar](50) NULL,
	[InsertDate] [Datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [Datetime] NULL,
	[UpdateAuditKey] [int] NULL
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC,
	[SourceRegion] ASC,
	[SourceProvider] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO

ALTER TABLE [dbo].[Employee]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_Manager] FOREIGN KEY([ManagerID], [ManagerSourceRegion], [ManagerSourceProvider])
REFERENCES [dbo].[Manager] ([ManagerID], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Manager]
GO
