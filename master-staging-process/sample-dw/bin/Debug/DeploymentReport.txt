﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [dbo].[DimAudit] (Table)
       Default Constraint: unnamed constraint on [dbo].[DimAudit] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[DimAudit] (Default Constraint)
       Default Constraint: unnamed constraint on [dbo].[DimAudit] (Default Constraint)
       [dbo].[FK_dbo_DimAudit_ParentAuditKey] (Foreign Key)

** Supporting actions
