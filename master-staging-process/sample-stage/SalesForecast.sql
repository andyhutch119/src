﻿CREATE TABLE [dbo].[SalesForecast] (
            [Version]                           NVARCHAR (50) NULL,
            [FullDate]                                  DATE NOT NULL,
            [BannerCode]                            NVARCHAR (50) NOT NULL,
            [CustomerID]                             NVARCHAR(50) NOT NULL,
            [ProductSKU]                            NVARCHAR(100) NOT NULL,
            [UnitForecast]                           DECIMAL (19, 4) NULL,
            [InvoicedSalesForecast] DECIMAL (19, 4) NULL,
            [COGForecast]                          DECIMAL (19, 4) NULL,
            [ListPriceForecast]                    DECIMAL (19, 4) NULL,
            [GrossMarginForecast]  DECIMAL (19, 4) NULL,
			[ManualRebateForecast]					DECIMAL(19,4),
            [SourceRegion]                         NVARCHAR(50) NOT NULL,
            [SourceProvider]                       NVARCHAR(50) NOT NULL
)


GO

CREATE INDEX [IX_SalesForecast_Column] ON [dbo].[SalesForecast] ( [BannerCode],  [CustomerID]  ,   [ProductSKU] )
