﻿CREATE TABLE [dbo].[BranchPlant] (
    [BranchPlant]     NVARCHAR (50)   NULL,
    [BranchPlantName] NVARCHAR (100)  NULL,
    [Company]         NVARCHAR (50) NULL,
	[SourceRegion]  NVARCHAR (50) NOT NULL,
	[SourceProvider] NVARCHAR (50) NOT NULL
);

