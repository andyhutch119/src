﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [dbo].[Product] (Table)
       [dbo].[Product].[IX_Product_Column] (Index)
       [dbo].[SalesForecast] (Table)
       [dbo].[SalesForecast].[IX_SalesForecast_Column] (Index)

** Supporting actions
